//
//  main.m
//  FlickrSearch
//
//  Created by A7med MAC on 2/6/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
