//
//  DataHandler.m
//  FlickrSearch
//
//  Created by A7med MAC on 2/6/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import "DataHandler.h"
#import "FlickrKit.h"
#import "MWPhoto.h"

#define FLICKR_API_KEY @"f905bbd9dc6cdb0b0616920c0f5cd189"
#define FLICKR_SECRET_KEY @"95f8ef1c32172ab1"

@implementation DataHandler

#pragma mark - Life Cycle
+ (DataHandler *) sharedInstance {
    
    static dispatch_once_t onceToken;
    
    static DataHandler *dataHandler = nil;
    
    dispatch_once(&onceToken, ^{
        
        dataHandler = [[self alloc] init];
    });
    
    return dataHandler;
}

- (id) init {

    self = [super init];
    
    if (self) {
        
        [[FlickrKit sharedFlickrKit] initializeWithAPIKey:FLICKR_API_KEY sharedSecret:FLICKR_SECRET_KEY];
    }
    
    return self;
}

#pragma mark - Search Method
- (void) searchForPhotosWithKeyword:(NSString *)keyword atPage:(NSInteger)pageNumber{

    FKFlickrPhotosSearch *search = [[FKFlickrPhotosSearch alloc] init];
    search.text = keyword;
    search.page = [NSString stringWithFormat:@"%i",pageNumber];
    search.per_page = @"501";
    
    [[FlickrKit sharedFlickrKit] call:search completion:^(NSDictionary *response, NSError *error) {
    
        if (response) {
            
            NSMutableArray *photos = [NSMutableArray array];
            
            for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                
                MWPhoto *photo = [MWPhoto photoWithURL:[[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeLarge1024 fromPhotoDictionary:photoDictionary]];
                photo.thumbURL = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeThumbnail100 fromPhotoDictionary:photoDictionary];
                photo.caption = photoDictionary[@"title"];
                [photos addObject:photo];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([self.delegate respondsToSelector:@selector(dataHandler:didFinishFetchResult:error:)]){
                    
                    [self.delegate dataHandler:self didFinishFetchResult:photos error:nil];
                }
            });

        } else {
            
            if([self.delegate respondsToSelector:@selector(dataHandler:didFinishFetchResult:error:)]){
                
                [self.delegate dataHandler:self didFinishFetchResult:nil error:error];
            }
        }
    }];
}

@end
