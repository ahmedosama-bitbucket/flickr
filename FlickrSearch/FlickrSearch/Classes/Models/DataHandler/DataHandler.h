//
//  DataHandler.h
//  FlickrSearch
//
//  Created by A7med MAC on 2/6/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataHandler;

@protocol DataHandlerDelegate <NSObject>

- (void) dataHandler:(DataHandler *) dataHandler didFinishFetchResult:(NSArray *) result error:(NSError *) error;

@end

@interface DataHandler : NSObject

@property (nonatomic, weak) id <DataHandlerDelegate> delegate;

+ (DataHandler *) sharedInstance;

- (void) searchForPhotosWithKeyword:(NSString *) keyword atPage:(NSInteger) pageNumber;

@end
