//
//  ViewController.m
//  FlickrSearch
//
//  Created by A7med MAC on 2/6/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import "PhotosViewController.h"
#import "HomeViewController.h"
#import "DataHandler.h"

@interface HomeViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // First initialization
    [self initialization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialization {

    self.title = @"Flickr";
    [_searchTextField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [self performSegueWithIdentifier:@"PhotosViewController" sender:nil];
    
    return YES;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"PhotosViewController"]) {
        
        PhotosViewController *photosViewController = (PhotosViewController *) segue.destinationViewController;
        photosViewController.searchKeyword = _searchTextField.text;
    }
}

@end