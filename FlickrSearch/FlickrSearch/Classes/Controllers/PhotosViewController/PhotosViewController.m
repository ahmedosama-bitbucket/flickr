//
//  PhotosViewController.m
//  FlickrSearch
//
//  Created by A7med MAC on 2/7/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotoCell.h"
#import "DataHandler.h"
#import "RTSpinKitView.h"
#import "MWPhotoBrowser.h"
#import "MWPhoto.h"
#import "UINavigationController+RadialTransaction.h"

@interface PhotosViewController () <UICollectionViewDataSource,UICollectionViewDelegate,DataHandlerDelegate,MWPhotoBrowserDelegate>

@property (nonatomic, strong) RTSpinKitView *spinKitView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic) NSInteger currentPage;

@end

@implementation PhotosViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // First initialization
    [self initialization];
    
    // Loading photos for |searchKeyboard| at first page
    [self loadPhotosAtPage:_currentPage];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initialization {

    _currentPage = 1;
    _photos = [NSMutableArray array];
    
    [[DataHandler sharedInstance] setDelegate:self];
    
    self.title = [NSString stringWithFormat:@"Result for '%@'",_searchKeyword];
    
    _spinKitView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[UIColor redColor]];
    _spinKitView.center = self.view.center;
    [self.view addSubview:_spinKitView];
    
    [_spinKitView startAnimating];
}

#pragma mark - Loading Data
- (void) loadPhotosAtPage:(NSInteger) page {

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [[DataHandler sharedInstance] searchForPhotosWithKeyword:_searchKeyword atPage:page];
    
    _currentPage ++;
}

#pragma mark - DataHandler
- (void) dataHandler:(DataHandler *)dataHandler didFinishFetchResult:(NSArray *)result error:(NSError *)error {
    
    [_spinKitView stopAnimating];

    [_photos addObjectsFromArray:result];
    [_collectionView reloadData];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return _photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell configureCellWithPhoto:_photos[indexPath.row]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    [self openPhotoBrowserAtIndex:indexPath.row];
}

#pragma mark - MWPhotoBrowserDelegate and actions
- (void) openPhotoBrowserAtIndex:(NSInteger) index {

    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [browser setAlwaysShowControls:NO];
    [browser setCurrentPhotoIndex:index];
    [self.navigationController radialPushViewController:browser withDuration:0.4 comlititionBlock:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {

    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {

    return _photos[index];
}

@end