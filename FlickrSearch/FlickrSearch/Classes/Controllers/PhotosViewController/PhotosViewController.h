//
//  PhotosViewController.h
//  FlickrSearch
//
//  Created by A7med MAC on 2/7/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosViewController : UIViewController

@property (nonatomic, strong) NSString *searchKeyword;

@end
