//
//  AppDelegate.h
//  FlickrSearch
//
//  Created by A7med MAC on 2/6/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

