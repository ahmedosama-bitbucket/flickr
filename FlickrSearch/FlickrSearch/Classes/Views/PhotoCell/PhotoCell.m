//
//  MWPhotoCell.m
//  FlickrSearch
//
//  Created by A7med MAC on 2/7/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import "PhotoCell.h"
#import "UIImageView+WebCache.h"
#import "DACircularProgressView.h"
#import "FBShimmering.h"
#import "FBShimmeringView.h"

@interface PhotoCell ()

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@end

@implementation PhotoCell

- (void)awakeFromNib {

    self.backgroundView.layer.cornerRadius = 2.0;
    self.imageView.layer.cornerRadius = 2.0;
}

- (void) configureCellWithPhoto:(MWPhoto *) photo {

    [_imageView sd_setImageWithURL:photo.thumbURL placeholderImage:[UIImage imageNamed:@"flickr2"]];
}

@end