//
//  MWPhotoCell.h
//  FlickrSearch
//
//  Created by A7med MAC on 2/7/15.
//  Copyright (c) 2015 iDream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhoto.h"

@interface PhotoCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *imageView;

- (void) configureCellWithPhoto:(MWPhoto *) photo;

@end
